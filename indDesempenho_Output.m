% C�lculo dos �ndices de Desempenho referente ao Sinal da Sa�da
% Nome: Jos� Oniram de A. Limaverde Filho
% Data: Agosto/18

function [ISE_y, IAE_y, ITAE_y] = indDesempenho_Output(Y, Yd, t, t0, dt)

  % Prepara��o das vari�veis
    tFinal = length(Y);

  % Integral dos Erros ao Quadrado (ISE - Integral Squared Error)
    ISE_y = 0;
    for i = t0:1:tFinal
      ISE_y = ISE_y + (Y(i) - Yd(i))^2*dt;
    end
    
    % Alternativa: ISE_y = trapz(t,(Y' - Yd).^2)     
      
  % Integral do M�dulo dos erros (IAE - Integral Absolute Error) 
    IAE_y = 0;
    for i = t0:1:tFinal
      IAE_y = IAE_y + abs(Y(i)-Yd(i))*dt;
    end
 
    % Alternativa: IAE_y = trapz(t, abs(Y' - Yd))
    
  % Integral do M�dulo dos erros ponderada pelo tempo (ITAE - Integral Time-weighted Absolute Error)
    ITAE_y = 0;
    for i = t0:1:tFinal
      ITAE_y = ITAE_y + t(i)*abs(Y(i) - Yd(i))*dt;
    end
    
    % Alternativa: ITAE_y = trapz(t, t.*abs(Y' - Yd))
