%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Fun��o utilizada para realizar a sintoniza��o do controlador PID
%%Mario Andr�s Pastrana Triana
%%Estudante de mestrado da UnB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function F = tracklsq(FlatValue, sp, t, Kbb, tau, dt, K1)

% Trajectory Planning
        
  % Flat Output R(t) and its time derivatives
  
    % Constant Trajectory
      b = sp; Rd = b + 0*t; d1Rd = 0*t; d2Rd = 0*t; d3Rd = 0*t; d4Rd = 0*t;
      
    % Cossenoidal Trajectory
      % ro = 0.05; w = 0.25;     
      % Rd = -ro*cos(w*t); d1Rd = ro*w*sin(w*t); d2Rd =  ro*(w^2)*cos(w*t); d3Rd = -ro*(w^3)*sin(w*t); d4Rd = -ro*(w^4)*cos(w*t); 
      
  % Nominal Trajectory - Theta(t)
    Thetad = asin(d2Rd/Kbb); d1Thetad = gradient(Thetad)/dt; d2Thetad = gradient(d1Thetad)/dt;
      
  % Nominal Control - Vm(t)
    Vm_d = (1/K1)*(tau*d2Thetad + d1Thetad);

  % Plotting Nominal Trajectories
    % plotNominal; return
    
% Controller Parameters
  %p1 = 1; P1 = poly(-[p1 p1 p1 p1]); 
  %k4 = P1(1,2); k3 = P1(1,3); k2 = P1(1,4); k1 = P1(1,5); % AJUSTA AQUI MARIO
  
  %Mediante a utiliza��o dos bioinspirados
  k4 = FlatValue(4);
  k3 = FlatValue(3);
  k2 = FlatValue(2);
  k1 = FlatValue(1);
  
  assignin('base', 'k1', k1);
  assignin('base', 'k2', k2);
  assignin('base', 'k3', k3);
  assignin('base', 'k4', k4);
  assignin('base', 'b', b);
  assignin('base', 'Rd', Rd);
  assignin('base', 'd1Rd', d1Rd);
  assignin('base', 'd2Rd', d2Rd);
  assignin('base', 'd3Rd', d3Rd);
  assignin('base', 'd4Rd', d4Rd);
  assignin('base', 'Thetad', Thetad);
  assignin('base', 'd1Thetad', d1Thetad);
  assignin('base', 'd2Thetad', d2Thetad);
  assignin('base', 'Vm_d', Vm_d);
  
% Simulink
    %disp('Simulink - Ball & Beam'); 
    sim('BB_flatness');
    % Prepara��o das vari�veis
    tFinal = length(rf);

  % Integral dos Erros ao Quadrado (ISE - Integral Squared Error)
    %ISE_y = 0;
    %for i = 1:1:tFinal
    %  ISE_y = ISE_y + (rf(i) - sp)^2*dt;
    %end
   
    % Integral do M�dulo dos erros (IAE - Integral Absolute Error) 
    %IAE_y = 0;
    %for i = 1:1:tFinal
    %  IAE_y = IAE_y + abs(rf(i)-sp)*dt;
    %end
 
    % Integral do M�dulo dos erros ponderada pelo tempo (ITAE - Integral Time-weighted Absolute Error)
    ITAE_y = 0;
    for i = 1:1:tFinal
      ITAE_y = ITAE_y + t(i)*abs(rf(i) - sp)*dt;
    end
  
      
    F=ITAE_y;    % Fun��o objetivo
  %plotResults;

    end