%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Algoritmo para fazer a sintonização do controlador PID utilizando PSO
%%João vitor Araujo dos Santos - Estudante de Engenharia Eletrônica da UNB
%%Lukas Araújo da Silva - Estudante de Engenharia Eletrônica da UNB
%%Mario Andrés Pastrana Triana Estudante de Doutorado da UnB
%%Mateus Souza Santana  Estudante de Engenheria Eletrônica da UnB 
%% Limpar o workspace e o command Windows
clear all    % Limpa o Workspace
close all    % Limpa as janelas
clc          % Limpa o Command Window
%% par�metros de configura��o do PSO
maxCycle = 350;                 % N�mero maximo de itera��es
runtime = 32;                   % N�mero maximo de experimentos
GlobalMins = zeros(1,runtime); % Minimos Globales
S = 15;                        % N�mero de part�culas
F = 1.25;                      % Factor de mutaci�n (caso seja executado o algoritmo DE)
C = 0.75;                      % crosover rate (caso seja executado o algoritmo DE)
x_min = 0.01;                 % Limite minimo do espa�o de busca
x_max = 20;                     % Limite maximo do espa�o de busca
N = 4;                         % N�mero de dimen��es (Kp, Ki, Kd)
ys = zeros(N);                 % Melhor global posi��es particulas
sp = 0;                       % SP utilizado para avaliar a fun��o objetivo
%%%%constant ball and beam
%%%%%%%%% global variables
%%%%%%%%%%%%%%
% Trajectory Tracking via Flatness Ball and Beam)
% Name: Jos� Oniram Limaverde

% Pr�-comandos
%clearvars; close all; clc

% Simulation Time
  t0 = 0; dt = 0.01; tfinal = 50;
  t = t0:dt:tfinal; st = size(t,2);
   
% Model Parameters
  m = 0.064;              % [Kg]
  Jb = 4.129*10^-6;       % [Kg]     
  R = 0.0127;             % [m]
  r_arm = 0.0254;         % [m]
  L_beam = 0.425;         % [m]
  g = 9.81;               % [m/s^2]

% Final Parameters
  Kbb = (m*(r_arm)*g*(R^2))/(L_beam*(m*R^2 + Jb));
  tau = 0.0248;
  K1  = 1.5286;
  b1  = -1/tau; 
  b2  = K1/tau;
     
% Saturation Limits

  % Control signal
    satV_sup = 10;  satV_inf = -satV_sup;
  
  % System Structure
    satAng = 56; % Beam Angle
  
% Initial Conditions

  % Ball position and Beam Angle
    r0 = -L_beam/2;  
    degAngInit = 0; theta0 = (degAngInit)*pi/180; 
  
  % Derivatives
    d1r0 = 0; 
    d1theta0 = 0;

%%%%
%% Execu��o do algoritmo PSO

for r=1:runtime     % For utilizado para executar o total de experimentos
        
    %[GlobalMins(r) ys] = PSO(x_min,x_max,N,S,maxCycle,sp, t, Kbb, tau, dt, K1);  % Execu��o do algoritmo PSO
    [GlobalMins(r) ys] = DE(x_min,x_max,S,N, maxCycle,sp,F,C, t, Kbb, tau, dt, K1);  % Execu��o do algoritmo DE
    %[GlobalMins(r) ys] = GOA(S, maxCycle,x_min,x_max,N,sp , t, Kbb, tau, dt, K1);  % Execu��o do algoritmo GOA
    %[GlobalMins(r) ys] = MFO(S,maxCycle,x_min,x_max,N,sp, t, Kbb, tau, dt, K1);  % Execu��o do algoritmo MFO
    save([ 'BAB_Optimization_DE_ITAE',num2str(r),'.mat']);  % Carrega os experimentos salvos
    disp('Esta no experimento:');                  % Apresenta Experimento
    disp(r); 
end                          % Finaliza��o dos experiementos

melhorglobal = 100000000;       % Melhor globlal fitnes 
%% Para calcular o melhor dos experimentos 
 for r=1:runtime     %For para o n�mero de experimentos

     %load([ 'BAB_Optimization_MFO_ITAE',num2str(r),'.mat']);  % Carrega os experimentos salvos
     if (GlobalMins(r)<= melhorglobal)  % Caso o GlobalMin seja  menor ao anterio salva em "mejorglobal"
         melhorglobal=GlobalMins(r);  % Salva GlobalMin em "mejorglobal"
         %k1=ys(1);          % Salva o valor do k1 melhor
         %k2=ys(2);          % Salva o valor do k2 melhor
         %k3=ys(3);          % Salva o valor do k3 melhor
         %k4=ys(4);          % Salva o valor do k3 melhor
         disp('Melhor experimento foi');                  % Apresenta Experimento
         disp(r);                              % Apresenta o numero de experimento 
     end
         
 end 