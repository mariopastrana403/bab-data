sz = size(tf)
sp = zeros(sz)
plot(tf(1:2500),rf_IAE(1:2500),"b")
hold on
plot(tf(1:2500),rf_ISE(1:2500),"r")
grid()
plot(tf(1:2500),rf_ITAE(1:2500),"g")
plot(tf(1:2500),rf_WRE(1:2500),"m")
plot(tf(1:2500),sp(1:2500),"k")
xlabel("Time (s)")
ylabel("R (m)")
legend("IAE-MFO", "ISE-MFO", "ITAE-GOA", "WRE-GOA")