% Display numerical simulation results

% Results visualization parameters
  close all; LW = 2;

% Ball Position (R)
  figure(1); 
  plot(t,Rd, 'b', 'LineWidth', LW); title('Ball Position', 'fontsize', 20); hold on;
  plot(tf,rf, 'r--', 'LineWidth', LW);  
  xlabel('t [s]', 'fontsize', 20); ylabel('R [m]', 'fontsize', 20);
  legend('Reference', 'Ball Position');

  
  
% Beam Angle (Theta)
  %figure(2);
  %plot(t,convang(Thetad, 'rad', 'deg'), 'b', 'LineWidth', LW); title('Beam Angle', 'fontsize', 20); hold on;
  %plot(tf,convang(Tf, 'rad', 'deg'), 'r--', 'LineWidth', LW);  
  %xlabel('t [s]', 'fontsize', 20); ylabel('\theta [rad]', 'fontsize', 20);
  %legend('Reference', 'Beam Angle');

% Control Signal (Motor Tension)
  %figure(3); 
  %plot(t,Vm_d, 'b', 'LineWidth', LW); title('Input Signal', 'fontsize', 20); hold on;
  %plot(tf,vmf, 'r--', 'LineWidth', LW);
  %xlabel('t [s]', 'fontsize', 20); ylabel('Vm [V]', 'fontsize', 20); 
  %legend('Reference', 'Input Signal');
