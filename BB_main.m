% Trajectory Tracking via Flatness Ball and Beam)
% Name: Jos� Oniram Limaverde

% Pr�-comandos
  clearvars; close all; clc

% Simulation Time
  t0 = 0; dt = 0.01; tfinal = 50;
  t = t0:dt:tfinal; st = size(t,2);
   
% Model Parameters
  m = 0.064;              % [Kg]
  Jb = 4.129*10^-6;       % [Kg]     
  R = 0.0127;             % [m]
  r_arm = 0.0254;         % [m]
  L_beam = 0.425;         % [m]
  g = 9.81;               % [m/s^2]

% Final Parameters
  Kbb = (m*(r_arm)*g*(R^2))/(L_beam*(m*R^2 + Jb));
  tau = 0.0248;
  K1  = 1.5286;
  b1  = -1/tau; 
  b2  = K1/tau;
     
% Saturation Limits

  % Control signal
    satV_sup = 10;  satV_inf = -satV_sup;
  
  % System Structure
    satAng = 56; % Beam Angle
  
% Initial Conditions

  % Ball position and Beam Angle
    r0 = -L_beam/2;  
    degAngInit = 0; theta0 = (degAngInit)*pi/180; 
  
  % Derivatives
    d1r0 = 0; 
    d1theta0 = 0;
   
% Trajectory Planning
        
  % Flat Output R(t) and its time derivatives
  
    % Constant Trajectory
      b = 0; Rd = b + 0*t; d1Rd = 0*t; d2Rd = 0*t; d3Rd = 0*t; d4Rd = 0*t;
      
    % Cossenoidal Trajectory
      % ro = 0.05; w = 0.25;     
      % Rd = -ro*cos(w*t); d1Rd = ro*w*sin(w*t); d2Rd =  ro*(w^2)*cos(w*t); d3Rd = -ro*(w^3)*sin(w*t); d4Rd = -ro*(w^4)*cos(w*t); 
      
  % Nominal Trajectory - Theta(t)
    Thetad = asin(d2Rd/Kbb); d1Thetad = gradient(Thetad)/dt; d2Thetad = gradient(d1Thetad)/dt;
      
  % Nominal Control - Vm(t)
    Vm_d = (1/K1)*(tau*d2Thetad + d1Thetad);

  % Plotting Nominal Trajectories
    % plotNominal; return
    
% Controller Parameters
  %p1 = 1; P1 = poly(-[p1 p1 p1 p1]); 
  %k4 = P1(1,2); k3 = P1(1,3); k2 = P1(1,4); k1 = P1(1,5); % AJUSTA AQUI MARIO
  
%Bioinspirados.

%Best bioinspired IAE MFO

% k1 = 17.8593
% k2 = 20
% k3 = 13.0804
% k4 = 2.8884


%Best bioinspired ISE MFO

% k1 = 20
% k2 = 18.7584
% k3 = 13.5922
% k4 = 1.9742

%Best bioinspired ITAE GOA

% k1 = 15.5691
% k2 = 20
% k3 = 12.8570
% k4 = 3.6092

%Best bioinspired WRE GOA

k1 = 18.0259
k2 = 20
k3 = 13.4156
k4 = 2.9842
           
% Simulink
  disp('Simulink - Ball & Beam'); 
  sim('BB_flatness');
  plotResults;

