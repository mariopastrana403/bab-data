% Trajet�rias Nominais para o modelo n�o-linear do Ball and Beam
% Nome: Jos� Oniram Limaverde

% Visualizando as trajet�rias desejadas
  figure(1); 
  subplot(2,2,1); plot(t,Rd); title('R(t) (Reference)');
  subplot(2,2,3); plot(t,d1Rd); title('d1R(t) (Reference)');
  subplot(2,2,2); plot(t,(180/pi)*Thetad); title('Theta(t) (Reference)');    
  subplot(2,2,4); plot(t,(180/pi)*d1Thetad); title('d1Theta(t) (Reference)');
  
  figure(2);
  plot(t,Vm_d); title('Input Signal (Reference) ');